package com.example.obs;

import com.example.obs.entity.ABolumEntity;
import com.example.obs.service.ABolumService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ObsApplicationTests {

	@Autowired
	private ABolumService aBolumService;

	@Test
	public void whenApplicationStarts_thenHibernateCreatesInitialRecords() {
		List<ABolumEntity> books = aBolumService.list();

		Assert.assertEquals(books.size(), 0);
	}
}
