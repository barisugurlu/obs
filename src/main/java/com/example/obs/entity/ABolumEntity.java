package com.example.obs.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "a_bolum", schema = "isg_katip_dev", catalog = "isg_katip_dev")
public class ABolumEntity {

    @GeneratedValue
    private Long bolumId;
    private String bolumAdi;
    private String aciklama;
    private String eposta;

    @Id
    @Column(name = "bolum_id")
    public Long getBolumId() {
        return bolumId;
    }

    public void setBolumId(Long bolumId) {
        this.bolumId = bolumId;
    }

    @Basic
    @Column(name = "bolum_adi")
    public String getBolumAdi() {
        return bolumAdi;
    }

    public void setBolumAdi(String bolumAdi) {
        this.bolumAdi = bolumAdi;
    }

    @Basic
    @Column(name = "aciklama")
    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    @Basic
    @Column(name = "eposta")
    public String getEposta() {
        return eposta;
    }

    public void setEposta(String eposta) {
        this.eposta = eposta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ABolumEntity that = (ABolumEntity) o;
        return bolumId == that.bolumId &&
                Objects.equals(bolumAdi, that.bolumAdi) &&
                Objects.equals(aciklama, that.aciklama) &&
                Objects.equals(eposta, that.eposta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bolumId, bolumAdi, aciklama, eposta);
    }
}
