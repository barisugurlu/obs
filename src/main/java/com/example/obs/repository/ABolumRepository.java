package com.example.obs.repository;

import com.example.obs.entity.ABolumEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ABolumRepository extends JpaRepository<ABolumEntity, Long> {
}
