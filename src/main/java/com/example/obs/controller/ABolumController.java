package com.example.obs.controller;

import com.example.obs.entity.ABolumEntity;
import com.example.obs.service.ABolumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class ABolumController {

    @Autowired
    ABolumService aBolumService;

    @GetMapping("/bolums")
    public List<ABolumEntity> getAllBolums() {
        return aBolumService.list();
    }
}
