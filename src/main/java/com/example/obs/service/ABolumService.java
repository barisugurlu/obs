package com.example.obs.service;

import com.example.obs.entity.ABolumEntity;
import com.example.obs.repository.ABolumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ABolumService {

    @Autowired
    ABolumRepository aBolumRepository;

    public List<ABolumEntity> list() {
        return aBolumRepository.findAll();
    }
}
